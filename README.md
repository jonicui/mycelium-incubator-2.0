The 3rd microchallenge involved using elements shown in the miro board below. 

<img src="images/miro.png"/>

Below you will find an outline showcasing our concept, planning, and execution of our mycelium incubator. 
Moving forward this project could be scaled up to great a larger incubator for bigger mycelium projects, but as it stands it's nice in that it's fairly small and transportable. 

The project was done by Pietro Rustici and Jasmine Boerner, repo's below. 
https://gitlab.com/pietro_rustici
https://gitlab.com/jasmine_boerner

<img src="images/1.jpg"/>

<img src="images/2.jpg"/>

<img src="images/3.jpg"/>

<img src="images/4.jpg"/>

<img src="images/5.jpg"/>

<img src="images/6.jpg"/>

<img src="images/7.jpg"/>

<img src="images/8.jpg"/>

<img src="images/9.jpg"/>

<img src="images/10.jpg"/>

<img src="images/11.png"/>

<img src="images/12.png"/>

<img src="images/13.png"/>

<img src="images/14.png"/>

<img src="images/15.png"/>

<img src="images/16.png"/>

<img src="images/17.png"/>

That's all folks!
