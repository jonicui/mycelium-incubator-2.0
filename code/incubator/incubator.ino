#include <WiFi.h>
#include <WebServer.h>
#include "Seeed_SHT35.h"
#include <uri/UriBraces.h>

// pins for the i2c temp+humiity sensor
#define SDAPIN  23
#define SCLPIN  22
#define RSTPIN  2
#define HEATERPIN 13
/* Put your SSID & Password */
const char* ssid = "MyceliumIncubator";  // Enter SSID here
const char* password = "12345678";  //Enter Password here

/* Put IP Address details */
IPAddress local_ip(192,168,1,1);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);

WebServer server(80);

bool incubator_status = LOW;
float temp,hum;
float temp_setpoint = 30;
int last_read = 0;

SHT35 sensor(SCLPIN);

void setup() {
  Serial.begin(115200);
  Serial.println("serial start!!");
  if(sensor.init()) Serial.println("sensor init failed!!!");
  delay(1000);
  
  pinMode(HEATERPIN, OUTPUT);

  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  delay(100);
  
  server.on("/", handle_OnConnect);
  server.on(UriBraces("/cycleon/{}"), []() {
    String s = server.pathArg(0);
    temp_setpoint = s.toFloat();
    incubator_status = HIGH;
    server.send(200, "text/html", SendHTML(temp,hum,temp_setpoint, incubator_status));
  });
  server.on("/cycleoff", handle_cycleoff);

  server.onNotFound(handle_NotFound);
  
  server.begin();
  Serial.println("HTTP server started");
}
void loop() {
  server.handleClient();
  if(millis() > (last_read + 1000)) {
    last_read = millis();
    updateTemp();
  }
  if(incubator_status) {
    if(temp >= temp_setpoint) digitalWrite(HEATERPIN, LOW);
    else digitalWrite(HEATERPIN, HIGH);
  } else {
    digitalWrite(HEATERPIN, LOW);
  }
}

void updateTemp() {
  u16 value=0;
  u8 data[6]={0};
  if(NO_ERROR!=sensor.read_meas_data_single_shot(HIGH_REP_WITH_STRCH,&temp,&hum)) {
    Serial.println("Read sensor failed!!");
  } else {
    Serial.println("read data :");
    Serial.print("temperature = ");
    Serial.print(temp);
    Serial.println(" ℃ ");
    Serial.print("humidity = ");
    Serial.print(hum);
    Serial.println(" % ");
    server.send(200, "text/html", SendHTML(temp,hum,temp_setpoint, incubator_status));
  }
}
void handle_OnConnect() {
  server.send(200, "text/html", SendHTML(temp,hum,temp_setpoint, incubator_status)); 
}


void handle_cycleoff() {
  incubator_status = LOW;
  server.send(200, "text/html", SendHTML(temp,hum,temp_setpoint, incubator_status));  
}


void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

String SendHTML(float temp,float humidity, float temp_setpoint, bool incubator_status){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>LED Control</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}\n";
  ptr +=".button {display: block;width: 80px;background-color: #3498db;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  ptr +=".button-on {background-color: #3498db;}\n";
  ptr +=".button-on:active {background-color: #4bd676;}\n";
  ptr +=".button-off {background-color: #34495e;}\n";
  ptr +=".button-off:active {background-color: #19ab46;}\n";
  ptr +="p {font-size: 14px;color: #888;margin-bottom: 10px;}\n";
  ptr +="</style>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<h1>Mycelium Incubator</h1>\n";
  ptr +="<h3>Control Interface</h3>\n";

  
  if(incubator_status) 
    ptr +="<p>Incubator status: ON</p><a class=\"button button-off\" href=\"/cycleoff\">Stop Cycle</a>\n";
  else {
    ptr +="<p>Incubator status: OFF</p><a id=\"setpa\" class=\"button button-on\" href=\"/cycleon/";
    ptr += temp_setpoint;
    ptr += "\">Start Cycle</a>\n";
  }
  ptr += "<p><input type=\"text\" id=\"setp\" onchange=\"setValue(this.value)\" value=\"";
  ptr += temp_setpoint;
  ptr += "\"></p>";
  ptr += "<p>Current temperature: <b>";
  ptr += temp;
  ptr += "</b> C</p>";
  
  ptr += "<p>Current humidity: <b>";
  ptr += humidity; 
  ptr += "</b> %</p>";
  
  ptr += "<p>Setpoint Temperature: <b>";
  ptr += temp_setpoint;
  ptr += "</b> C</p>";

  ptr += "<script>";
  ptr += "function setValue(val) {";
  ptr += "console.log(val);";
  ptr += "document.getElementById(\"setpa\").href=\"cycleon/\" + val;";
  ptr += "} </script>";
  
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}
